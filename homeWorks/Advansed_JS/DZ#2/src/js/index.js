`use strict`

// ## Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию `try...catch`.
//
// Конструкцию `try...catch` нужно применять везде, где мы получаем какие либо данные из-вне.
//

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

let root = document.getElementById("root");
let ul = document.createElement("ul");

root.append(ul);

for(let i = 0; i < books.length; i++){
  try {
    let book = books[i];
    if (!book.author) {
      throw new Error("Incomplete data: no author");
    }
    if (!book.name) {
      throw new Error("Incomplete data: no name");
    }
    if (!book.price) {
      throw new Error("Incomplete data: no price");
    }else if(book.author && book.name && book.price){
      ul.append(createLi(book));
    }
  }catch(e){
    console.log(e);
  }
}

function createLi(obj){
  ( {author, name, price} = obj);
  let li = document.createElement("li");
  let p1 = document.createElement("p");
  let p2 = document.createElement("p");
  let p3 = document.createElement("p");

  p1.textContent = `author: "${author}"`;
  p2.textContent = `name: "${name}"`;
  p3.textContent = `price: ${price}`;

  li.append(p1);
  li.append(p2);
  li.append(p3);
  return li;
}
