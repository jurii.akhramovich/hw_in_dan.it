`use strict`

// ## Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//
// Прототипное наследование - это возможность создавать новые объекты/классы, основанные на каком-то классе/объекте-родителе,
// использовать его методы и иметь возможнось расширить этот объект/класс собственным функционалом
//

class Employee{
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(name){
    this._name = name;
  }
  get name(){
    return this._name;
  }

  set age(age){
    this._age = age;
  }
  get age(){
    return this._age;
  }

  set salary(salary){
    this._salary = salary;
  }
  get salary(){
    return this._salary;
  }
}

class Programmer extends Employee{
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set salary(salary){
    this._salary = salary * 3;
  }
  get salary() {
    return this._salary;
  }
}

let programmer1 = new Programmer("John", 23, 1500, ["HTML", "CSS"]);
let programmer2 = new Programmer("Jack", 27, 2500, ["HTML", "CSS", "JS"]);
let programmer3 = new Programmer("Ivan", 30, 3500, ["HTML", "CSS", "JS", "Java", "PHP"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);