`use strict`

// ## Теоретический вопрос
// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
//
// Прототипное наследование - это возможность создавать новые объекты/классы, основанные на каком-то классе/объекте-родителе,
// использовать его методы и иметь возможнось расширить этот объект/класс собственным функционалом
//

let baseURL = "https://swapi.dev/api/";
let filmsQuery = baseURL + "films/";
let peopleQuery = baseURL + "people/";

fetch(filmsQuery)
    .then((response) => response.json())
    .then((data) => {
      let {results} = data;
      filmPusher(results);
    })
    .catch((error) => console.log(error.message));



function listCreator() {
  let ul = document.createElement("ul");
  let li = document.createElement("li");
  let title = document.createElement("h3");
  let episode = document.createElement("p");
  let openingCrawl = document.createElement("p");
  li.append(episode, title, openingCrawl);
  return { ul, li };
};

function filmPusher(results) {
  const { ul, li } = listCreator();
  results.forEach(({ title, episode_id, opening_crawl, characters}) => {
    let listItem = li.cloneNode(true);
    listItem.children[1].textContent = title;
    listItem.children[0].textContent = episode_id;
    listItem.children[2].textContent = opening_crawl;
    listItem.children[2].prepend(creatorListOfActors(characters));
    ul.append(listItem);
  });
  document.getElementById("root").append(ul);
};

function  creatorListOfActors(characters){
  const actors = document.createElement("ul");
  for(let i = 1; i <= characters.length; i++){
    if(i != 17) {
      fetch(peopleQuery + i + "/")
          .then((response) => response.json())
          .then(({name}) => {
              let li = document.createElement("li");
              li.textContent = name;
              actors.append(li);
          })
          .catch((error) => console.log(error.message));
    }
  }
  return actors;
}
