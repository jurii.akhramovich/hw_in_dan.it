`use strict`

// ## Теоретический вопрос
// Обьясните своими словами, как вы понимаете асинхронность в Javascript
//
// Асинхронность - это модель поведения, которая позволяет продолжить выполнение следующего блока кода, пока данный блок
// получает или обрабатывает данные
//

let getIP = "https://api.ipify.org/?format=json";
let apiInfo = "http://ip-api.com/json/";
let query = "?fields=continent,country,regionName,city,district";

const btn = document.getElementById("findByIP");

btn.addEventListener("click", findIP);

async function findIP(){
    await fetch(getIP)
        .then((response) => response.json())
        .then(({ip}) => getInfoByIP(ip))
        .catch((error) => console.log(error.message));
}

async function getInfoByIP(ip) {
    await fetch(apiInfo + ip + query)
        .then((response) => response.json())
        .then((data) => listCreator(data))
        .catch((error) => console.log(error.message));
}

function listCreator(data) {
    let {continent, country, regionName, city, district} = data;
    const ul = document.createElement("ul");
    const continentField = document.createElement("li");
    const countryField = document.createElement("li");
    const regionNameField = document.createElement("li");
    const cityField = document.createElement("li");
    const districtField = document.createElement("li");

    continentField.textContent = "continent: " + continent;
    countryField.textContent = "country: " + country;
    regionNameField.textContent = "region: " + regionName;
    cityField.textContent = "city: " + city;
    districtField.textContent = "district: " + district;

    ul.append(continentField, countryField, regionNameField, cityField, districtField);
    document.getElementById("root").append(ul);
}