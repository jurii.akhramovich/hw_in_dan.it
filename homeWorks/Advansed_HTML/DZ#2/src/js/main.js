const menuBtn = document.querySelector(".menu-btn");
const menuBox = document.querySelector(".menu");

menuBtn.addEventListener("click", (event) => {
    menuBtn.classList.toggle("menu-btn--active");
    menuBox.classList.toggle("menu--visible");
});