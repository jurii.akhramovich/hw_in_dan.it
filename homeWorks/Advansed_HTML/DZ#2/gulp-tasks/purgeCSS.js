const {src, dest} = require("gulp");
const cleanUnCSS = require('gulp-purgecss');

const purgeCSS = () => {
    return src("./dist/css/styles.min.css")
        .pipe(cleanUnCSS({
            content: ["index.html", "./src/js/*.js"]
        }))
        .pipe(dest("./dist/css/"));
};

exports.purgeCSS = purgeCSS;