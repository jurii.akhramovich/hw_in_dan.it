const {src, dest} = require("gulp");
const sass = require("gulp-sass");

const styles = () => {
    return src("./src/scss/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(dest("./dist/css/"));
};

const stylesMin = () => {
    return src("./src/scss/*.scss")
        .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
        .pipe(dest("./dist/css/"));
};

exports.styles = styles;
exports.stylesMin = stylesMin;