const {src, dest} = require("gulp");
const prefixer = require("gulp-autoprefixer");

const autoPrefixer = () => {
    return src("./dist/css/styles.min.css")
        .pipe(prefixer({
            overrideBrowserslist: ["last 2 versions"],
            cascade: false
        }))
        .pipe(dest("./dist/css"));
};

exports.autoPrefixer = autoPrefixer;