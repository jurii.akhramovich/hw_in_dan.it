 bconst {watch} = require("gulp");
const scriptsTask = require("./scripts").scripts;
const stylesTask = require("./styles").styles;
const browserSync = require("./serve").browserSync;
const imageMin = require("./imageMin").imageMin;

const watcher = () => {
    watch("./index.html", (cb) => {
        browserSync.reload();
        cb();
    });
    watch("./src/js/*.js", (cb) => {
        scriptsTask();
        browserSync.reload();
        cb();
    });
    watch("./src/scss/*.scss", (cb) => {
        stylesTask();
        browserSync.reload();
        cb();
    });
    watch("./src/img/**", (cb) => {
        imageMin();
        browserSync.reload();
        cb();
    });
};

exports.watch = watcher;