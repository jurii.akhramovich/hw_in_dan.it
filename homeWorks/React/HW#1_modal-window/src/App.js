import React, {PureComponent} from 'react';
import './styles/App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends PureComponent {
    state = {
        firstModalOpened: false,
        secondModalOpened: false
    }

    toggleFirstModal = (event) => {
        if (event.target.dataset.action === "toggle-modal") {
            this.setState({firstModalOpened: !this.state.firstModalOpened});
        }
    }

    toggleSecondModal = (event) => {
        if (event.target.dataset.action === "toggle-modal") {
            this.setState({secondModalOpened: !this.state.secondModalOpened});
        }
    }

    render() {
        const firstModal = <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you delete this file, it won’t be possible to undo this action.
                  Are you sure you want to delete it?"
            actions={
                <section>
                    <Button backgroundColor="#b3382c" text="Ok"/>
                    <Button backgroundColor="#b3382c" text="Cancel"/>
                </section>
            }
            closeHandler={this.toggleFirstModal}
        />;
        const secondModal = <Modal
            header="Are you sure?"
            closeButton={true}
            text="Lorem ipsum dolor sit amet, consectetur adipisicing?"
            actions={
                <section>
                    <Button backgroundColor="#359fe9" text="Yes"/>
                    <Button backgroundColor="#359fe9" text="No"/>
                </section>
            }
            closeHandler={this.toggleSecondModal}
        />;

        const {firstModalOpened, secondModalOpened} = this.state;
        return (
            <div className="App">
                <Button backgroundColor="#4635e9" text="Open first modal" onClick={this.toggleFirstModal}/>
                <Button backgroundColor="#e9e735" text="Open second modal" onClick={this.toggleSecondModal}/>
                {firstModalOpened && firstModal}
                {secondModalOpened && secondModal}
            </div>
        );
    }
}

export default App;