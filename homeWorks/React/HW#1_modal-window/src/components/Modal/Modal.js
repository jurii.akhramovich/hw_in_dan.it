import React, {PureComponent} from 'react';
import './Modal.scss';

class Modal extends PureComponent {
    render() {
        const {header, closeButton, text, actions, closeHandler} = this.props;
        return (
            <div className="modal-coverage" onClick={closeHandler} data-action="toggle-modal">
                <div className="modal">
                    <h3 className="modal__header">{header}</h3>
                    {closeButton && <div className="close-btn" data-action="toggle-modal"></div>}
                    <p className="modal__body">{text}</p>
                    {actions}
                </div>
            </div>
        );
    }
}

export default Modal;