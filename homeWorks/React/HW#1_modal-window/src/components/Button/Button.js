import React, {PureComponent} from 'react';
import styled from "styled-components";

const StyledButton = styled.button`
  min-width: 100px;
  margin: 0 10px;
  border: 0;
  border-radius: 4px;
  padding: 10px;
  cursor: pointer;
  background-color: ${props=> props.backgroundColor};
  color: #ffffff;
  
  &:focus {
    outline: none;
  }
`;

class Button extends PureComponent {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (
            <StyledButton backgroundColor = {backgroundColor} onClick={onClick} data-action="toggle-modal">
                {text}
            </StyledButton>
        );
    }
}

export default Button;