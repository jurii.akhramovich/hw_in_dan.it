"use strict";

//## Теоретический вопрос
/*
* Циклы используют для выполнения однотипных, повторяющихся действий, чтобы не дублировать код.
*/

let div = false;
let a;
let b;

do{
    a = +prompt("Enter first number");
    b = +prompt("Enter second number");

    let m = null;
    let n = null;

    if(a < b) {
        m = a;
        n = b;
    }else{
        m = b;
        n = a;
    }

    if(Number.isInteger(a) && Number.isInteger(b)){

        function testSimpleNumber(num) {
            if (num < 2) {
                return false;
            }else if (num === 2) {
                return true;
            }else{
                let i = 2;
                let limit = Math.sqrt(num);
                while (i <= limit) {
                    if (num % i === 0) {
                        return false;
                    }
                    i +=1;
                }
            }
            return true;
        }

        for(let i = m; i <= n; i++){
            if(testSimpleNumber(i)){
                console.log(i);
            }
        }
    }else{
        console.log("Error!!!");
    }
}while(!Number.isInteger(a) || !Number.isInteger(b));