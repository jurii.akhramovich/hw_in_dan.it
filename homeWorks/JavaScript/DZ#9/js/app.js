"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
* Это функция, в которой описаны действия, которые должны выполняться при возникновении данного события.
*
* */

const menu = Array.from(document.querySelectorAll(".tabs-title"));
const content = document.querySelector(".tabs-content");
const discriptions = Array.from(content.getElementsByTagName("li"));

discriptions[0].style.cssText = "display: block";

function deleteClass(arr){
    arr.map((item)=>{
        if (item.classList.contains("active")) {
            item.classList.remove("active");
        }
    })
}
function deleteDiscription(arr){
    arr.map((item)=>{
        item.style.cssText = "display: none";
    })
}

for (let i = 0; i < (Array.from(menu).length); i++) {
    menu[i].addEventListener("click", function () {
        deleteClass(menu);
        deleteDiscription(discriptions);
        menu[i].classList.add("class", "active");
        discriptions[i].style.cssText = "display: block";

    })
}
