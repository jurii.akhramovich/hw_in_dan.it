"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
* Это объектная модель документа, в которой каждый HTML-тег или вложенный в него текст является объектом.
*
* */

let array = ['hello', {name: "Ivan", age: 18, lastName: "Ivanov"}, 'world', 'Kiev', ["1", '2', 3, 8], 'Kharkiv', 'Odessa', 'Lviv'];
let root = document.getElementById("root");
const fragmentHTML = document.createDocumentFragment();

function createList(arr){
    const ulMain = document.createElement("ul");
    root.append(ulMain);

    for (const element of arr) {
        if(isArray(element)){
            let ul = document.createElement("ul");
            for (const key of element){
                let li = document.createElement("li");
                li.textContent = key;
                fragmentHTML.append(li);
            }
            ul.append(fragmentHTML);
            ulMain.append(ul);
        }else if(typeof element === "object"){
            let ul = document.createElement("ul");
            for (const key in element) {
                let li = document.createElement("li");
                li.textContent = `${key}: ${element[key]}`;
                fragmentHTML.append(li);
            }
            ul.append(fragmentHTML);
            ulMain.append(ul);

        }else {
            let li = document.createElement("li");
            li.textContent = element;
            ulMain.append(li);
        }
    }
    function isArray(value) {
        return Object.prototype.toString.call(value) === '[object Array]';
    }
}

function cleanPage(n){
    let par = document.createElement("p");
    par.textContent = `List will deleted for 10 seconds`;
    root.append(par);

    setTimeout(() => {}, 4000);
    setTimeout(function tick () {
        par.textContent = "";
        par.textContent = n;
        if (n < 0) {
            root.textContent = "";
            return;
        }
        n--;

        setTimeout(tick, 1000);
    }, 1000)
}

console.log(createList(array));

cleanPage(10);