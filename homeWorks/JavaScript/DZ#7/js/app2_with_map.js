"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
* Это объектная модель документа, в которой каждый HTML-тег или вложенный в него текст является объектом.
*
* */

let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let root = document.getElementById("root");
const fragmentHTML = document.createDocumentFragment();

function createList(arr){
    const ulMain = document.createElement("ul");
    root.append(ulMain);

    let newArr = arr.map((item) => {
        let li = document.createElement("li");
        li.textContent = item;
        return li;
    });

    fragmentHTML.append(...newArr);
    ulMain.append(fragmentHTML);
}

function cleanPage(n){
    let par = document.createElement("p");
    par.textContent = `List will deleted for 10 seconds`;
    root.append(par);

    setTimeout(() => {}, 4000);
    setTimeout(function tick () {
        par.textContent = "";
        par.textContent = n;
        if (n < 0) {
            root.textContent = "";
            return;
        }
        n--;

        setTimeout(tick, 1000);
    }, 1000)
}

createList(array);

cleanPage(10);