"use strict";

// ## Теоретический вопрос

/*
*
* 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
* События клавиатуры предназначены именно для работы с клавиатурой - их можно использовать для проверки ввода в <input>, но текст может быть вставлен мышкой, при помощи правого клика и меню, без единого нажатия клавиши, поэтому могут возникнуть ошибки.
*/

const allBtn = document.querySelectorAll(".btn");

document.addEventListener("keyup", (evt)=> {

    Array.from(allBtn).forEach((item) => {
        item.classList.remove("active");
    });

    switch (evt.code){
        case "Enter":
            allBtn[0].classList.toggle("active");
            break;
        case "KeyS":
            allBtn[1].classList.toggle("active");
            break;
        case "KeyE":
            allBtn[2].classList.toggle("active");
            break;
        case "KeyO":
            allBtn[3].classList.toggle("active");
            break;
        case "KeyN":
            allBtn[4].classList.toggle("active");
            break;
        case "KeyL":
            allBtn[5].classList.toggle("active");
            break;
        case "KeyZ":
            allBtn[6].classList.toggle("active");
            break;
    }
})


