"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами как работает цикл forEach.
*  Это цикл для массивов - он перебирает все элементы массива (проходит по всем).
*
* */

const arr = ['hello', 6, undefined, ['string', 'number'], 'world', 23, '23', null];
const type = 'string';

console.log(arr.filter(e => typeof e !== type));