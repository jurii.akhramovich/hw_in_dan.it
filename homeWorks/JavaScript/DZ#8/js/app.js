"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
* Это функция, в которой описаны действия, которые должны выполняться при возникновении данного события.
*
* */

const root = document.getElementById("root");
const input = document.getElementById("price");

let spanErr = document.createElement("span");
let span = document.createElement("span");
let btn = document.createElement("button");

function isNumber(num){
    return (!isNaN(+num) && num !== null && num !== "");
}
function clear(){
    span.remove();
    btn.remove();
    spanErr.textContent = "";
}

const focusHandler = function(){
    input.style.cssText = "border: 2px solid green";
}

const blurHandler = function(){
    input.style.cssText = "";
    let value = input.value;
    if(!isNumber(value) || +input.value < 0){
        clear();
        input.style.cssText = "border: 2px solid red";
        spanErr.textContent = "Please enter correct price";
        root.after(spanErr);
    }else {
        clear();
        input.style.color = "green";
        span.textContent = `Текущая цена: ${value}`;
        span.classList.add("bottom-span");
        btn.textContent = "X";
        btn.classList.add("btn");
        root.before(span);
        span.append(btn);

        btn.addEventListener("click", () => {
            span.remove();
            btn.remove();
            input.setAttribute("value", "");
        });
    }
}

input.addEventListener("focus", focusHandler);
input.addEventListener("blur", blurHandler);