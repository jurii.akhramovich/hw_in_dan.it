"use strict";

// ## Теоретический вопрос

/*
*
* 1. Опишите своими словами, что такое метод объекта
* Методы объекта — это функции, закрепленные за этим объектом. Тоесть наборы действий и операций
*
* */

function createNewUser(){
    const newUser = {
        getLogin(firstName, lastName){
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        },
        setFirstName(firstName){
            this.firstName = firstName;
        },
        getFirstName(){
            return this.firstName;
        },
        setLastName(lastName){
            this.lastName = lastName;
        },
        getLastName(){
            return this.lastName;
        },
    };
    newUser.setFirstName(prompt("Enter your first name"));
    newUser.setLastName(prompt("Enter your last name"));
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());