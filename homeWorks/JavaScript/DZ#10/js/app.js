"use strict";

const password = document.querySelector("#password");
const confirm = document.querySelector("#confirm");
const button = document.querySelector("#button");
const iconPas = document.querySelector("#password-icon");
const iconConf = document.querySelector("#confirm-icon");
const p = document.querySelector("#error");

iconPas.addEventListener("click", ()=> {
    showHideIcon(iconPas);
    showHidePass(password);
});

iconConf.addEventListener("click", ()=> {
    showHideIcon(iconConf);
    showHidePass(confirm);
});

button.addEventListener("click", (e)=>{
    p.style.visibility = "hidden";
    e.preventDefault();
    setTimeout(()=> {
        if(password.value === confirm.value){
            alert("You are welcome");
        }else{
            p.style.visibility = "visible";
        }
    });
});

function showHidePass(input) {
    p.style.visibility = "hidden";
    if (input.getAttribute('type') === 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
    }
}
function showHideIcon(icon){
    if (icon.classList.contains("fa-eye-slash")) {
        icon.classList.replace("fa-eye-slash", "fa-eye");
    } else {
        icon.classList.replace("fa-eye", "fa-eye-slash");
    }
}